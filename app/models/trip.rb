class Trip < ActiveRecord::Base
	has_many :trip_images, dependent: :destroy
	accepts_nested_attributes_for :trip_images, allow_destroy: true

end
