class TripImage < ActiveRecord::Base
	attr_accessor :photo
	belongs_to :trip
	has_attached_file :photo,
	path: ":rails_root/public/images/:id/:filename",
	url:  "/images/:id/:filename",
	styles: { 
		#resize imagen full ancho y alto fijo
		# small: ["100x100!", :jpg],
		# medium: ["250x250!"],
		# large: ["500x500!", :jpg]
		#crop imagen 
		# small: ["100x100#", :jpg, quality: 70],
		# medium: ["250x250#"],
		# large: ["500x500#", :jpg, quality: 70]
		# resize imagen proporcional 
		small: "100x100>",
		medium: "250x250>",
		large: "500x500>",
		extralarge: "1280x1000>",
		
		},
		convert_options: {
			all: '-quality 75',
			# small: '-colorspace Gray -resize 200x200 ',
			# medium: '-colorspace Gray -resize 200x200 ',
			# large: '-radial-blur 4 -resize 200x200 '
		}
		validates_attachment_file_name :photo, matches: [/png\Z/, /jpe?g\Z/]

	end
