class TripImagesController < ApplicationController
  before_action :set_trip_image, only: [:show, :edit, :update, :destroy]

  # GET /trip_images
  # GET /trip_images.json
  def index
    @trip_images = TripImage.all
  end

  # GET /trip_images/1
  # GET /trip_images/1.json
  def show
  end

  # GET /trip_images/new
  def new
    @trip_image = TripImage.new
  end

  # GET /trip_images/1/edit
  def edit
  end

  # POST /trip_images
  # POST /trip_images.json
  def create
    @trip_image = TripImage.new(trip_image_params)

    respond_to do |format|
      if @trip_image.save
        format.html { redirect_to @trip_image, notice: 'Trip image was successfully created.' }
        format.json { render :show, status: :created, location: @trip_image }
      else
        format.html { render :new }
        format.json { render json: @trip_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trip_images/1
  # PATCH/PUT /trip_images/1.json
  def update
    respond_to do |format|
      if @trip_image.update(trip_image_params)
        format.html { redirect_to @trip_image, notice: 'Trip image was successfully updated.' }
        format.json { render :show, status: :ok, location: @trip_image }
      else
        format.html { render :edit }
        format.json { render json: @trip_image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trip_images/1
  # DELETE /trip_images/1.json
  def destroy
    @trip_image.destroy
    respond_to do |format|
      format.html { redirect_to trip_images_url, notice: 'Trip image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip_image
      @trip_image = TripImage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_image_params
      params.require(:trip_image).permit(:trip_id)
    end
end
