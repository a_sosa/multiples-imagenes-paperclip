class TripsController < ApplicationController
  before_action :set_trip, only: [:show, :edit, :update, :destroy]

  # GET /trips
  # GET /trips.json
  def index
    @trips = Trip.all
  end

  # GET /trips/1
  # GET /trips/1.json
  def show
    @gallery  = Trip.find(params[:id])
    @pictures = @gallery.trip_images
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gallery }
    end

  end

  # GET /trips/new
  def new
    @trip = Trip.new
    @trip.trip_images.build
  end

  # GET /trips/1/edit
  def edit
    @trip = Trip.find(params[:id])
    5.times {@trip.trip_images.build}
    
  end



  # POST /trips
  # POST /trips.json
  def create
    @trip = Trip.new(title:  params[:title])
    respond_to do |format|
      if @trip.save
        if params[:photos]
          # The magic is here ;)
          params[:photos].each { |image|
            @trip.trip_images.create(photo: image)
          }
        end
        format.html { redirect_to @trip, notice: 'Trip was successfully created.' }
        format.json { render :show, status: :created, location: @trip }
      else
        format.html { render :new }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trips/1
  # PATCH/PUT /trips/1.json
  def update
    respond_to do |format|
      if @trip.update(trip_params)
        format.html { redirect_to @trip, notice: 'Trip was successfully updated.' }
        format.json { render :show, status: :ok, location: @trip }
      else
        format.html { render :edit }
        format.json { render json: @trip.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trips/1
  # DELETE /trips/1.json
  def destroy
    @trip.destroy
    respond_to do |format|
      format.html { redirect_to trips_url, notice: 'Trip was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trip
      @trip = Trip.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trip_params
      params.require(:trip).permit(:title)
    end
end
