json.array!(@trip_images) do |trip_image|
  json.extract! trip_image, :id, :trip_id
  json.url trip_image_url(trip_image, format: :json)
end
