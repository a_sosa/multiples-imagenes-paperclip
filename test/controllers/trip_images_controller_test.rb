require 'test_helper'

class TripImagesControllerTest < ActionController::TestCase
  setup do
    @trip_image = trip_images(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:trip_images)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trip_image" do
    assert_difference('TripImage.count') do
      post :create, trip_image: { trip_id: @trip_image.trip_id }
    end

    assert_redirected_to trip_image_path(assigns(:trip_image))
  end

  test "should show trip_image" do
    get :show, id: @trip_image
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trip_image
    assert_response :success
  end

  test "should update trip_image" do
    patch :update, id: @trip_image, trip_image: { trip_id: @trip_image.trip_id }
    assert_redirected_to trip_image_path(assigns(:trip_image))
  end

  test "should destroy trip_image" do
    assert_difference('TripImage.count', -1) do
      delete :destroy, id: @trip_image
    end

    assert_redirected_to trip_images_path
  end
end
