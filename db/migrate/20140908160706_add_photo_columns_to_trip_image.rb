class AddPhotoColumnsToTripImage < ActiveRecord::Migration
  def self.up
    add_attachment :trip_images, :photo
  end

  def self.down
    remove_attachment :trip_images, :photo
  end
end
